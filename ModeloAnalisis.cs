using System;

namespace WebApiOracle
{
    public class ModeloAnalisis
    {

        public Int32 P_VALIDACION { get; set; }

        public Int32 P_IDENTIFICADOR { get; set; }

        public String P_CUNU { get; set; }

        public String P_DIRECCION { get; set; }

        public String P_ID_CIUDANANO { get; set; }

        public String P_R_NOMBARIO { get; set; }

        public String P_R_LOCALIDAD { get; set; }

        public String P_R_ESTRATO { get; set; }



    }
}
