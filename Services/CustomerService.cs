﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Interface;
using WebApiOracle.Models;
using Dapper.Oracle;

namespace WebApiOracle.Services
{
    public class CustomerService : ICustomerService
    {

        private readonly string _connectionString;

        // IConfiguration configuration;

        public CustomerService(IConfiguration _configuration) {
            _connectionString = _configuration.GetConnectionString("OracleDBConnection");
        }

        public IDbConnection GetConnection() {            
            var conn = new OracleConnection(_connectionString);
            return conn;
        }

        public object GetListCustomer()
        {


            object result = null;
            try
            {

                var dynamicParameters = new OracleDynamicParameters();
                //OracleDynamicParameters dynamicParameters = new OracleDynamicParameters();


                dynamicParameters.Add(":cursorConsumer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "select * from BINTABLAS WHERE ROWNUM <= 10";
                    result = SqlMapper.Query(conn, query, commandType: CommandType.Text);


                    //var query = "CONSULTAR_CONSUMER";

                    //result = SqlMapper.Query(conn, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);



                    //OracleDynamicParameters dynamicParameters = new OracleDynamicParameters();

                    //dynamicParameters.Add(name: ":cursorConsumer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                    //result = conn.Query<Customer>("CONSULTAR_PRODUCTO", param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();





                    //games = this._dbConnection.Query<GameCatalog>("atlantisgames.getallgames", param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public object AddSolicitante(ModeloSaveSolicitante modeloSaveSolicitante)
        {
            object saveSolicitante = null;

            try
            {
                var dynamicParameters = new OracleDynamicParameters();

                dynamicParameters.Add("P_NOMBRE1", modeloSaveSolicitante.PARAM_NOMBRE1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_NOMBRE2", modeloSaveSolicitante.PARAM_NOMBRE2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_APELLIDO1", modeloSaveSolicitante.PARAM_APELLIDO1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_APELLIDO2", modeloSaveSolicitante.PARAM_APELLIDO2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_TIPO_DOCUMENTO", modeloSaveSolicitante.PARAM_TIPO_DOCUMENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_NO_DOCUMENTO", modeloSaveSolicitante.PARAM_NO_DOCUMENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_SEXO", modeloSaveSolicitante.PARAM_SEXO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_FECHA_NACIMIENTO", modeloSaveSolicitante.PARAM_FECHA_NACIMIENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_CORREO_ELECTRONICO", modeloSaveSolicitante.PARAM_CORREO_ELECTRONICO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_TELEFONO", modeloSaveSolicitante.PARAM_TELEFONO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                dynamicParameters.Add("P_PARENTESCO", modeloSaveSolicitante.PARAM_PARENTESCO, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input, size: 50);
                dynamicParameters.Add("P_DIRECCION", modeloSaveSolicitante.PARAM_DIRECCION, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 100);

                //string data_cunu = String(modeloSaveSolicitante.PARAM_CUNU);
                //string data_cunu = "0CL065b0000008800000520";


                dynamicParameters.Add("P_CUNU", modeloSaveSolicitante.PARAM_CUNU, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                //dynamicParameters.Add("P_CUNU", "0CL012C00000071B0000400", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);


                dynamicParameters.Add("P_INTERNO_CIUDADANO", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output); ;
                dynamicParameters.Add("P_NUMERO_CONSECUTIVO", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                var conn = this.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "SB.PR_REGISTRO_SOLICITANTE";

                    SqlMapper.Query(conn, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);

                    var dato_interno = dynamicParameters.Get<Int32>("P_INTERNO_CIUDADANO");

                    var dato_consecutivo = dynamicParameters.Get<Int32>("P_NUMERO_CONSECUTIVO");

                    var arrayResponseSolicitante = new RespuestaSolicitante
                    {
                        P_INTERNO_CIUDADANO = dato_interno,
                        P_NUMERO_CONSECUTIVO = dato_consecutivo,
                    };

                    saveSolicitante = arrayResponseSolicitante;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return saveSolicitante;
        }

        public List<RespuestaGrupoFamiliar> AddGrupoFamiliar(List<ModeloSaveGrupoFamiliar> modeloSaveGrupoFamiliar)
        {
            object saveGrupoFamiliar = null;

            List<RespuestaGrupoFamiliar> saveGrupoFamiliarArray = new List<RespuestaGrupoFamiliar> ();

            try
            {
                foreach (ModeloSaveGrupoFamiliar lst in modeloSaveGrupoFamiliar)
                {
                    var dynamicParameters = new OracleDynamicParameters();

                    dynamicParameters.Add("P_INTERNO_CIUDADANO", lst.PARAM_INTERNO_CIUDADANO, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_NOMBRE1", lst.PARAM_NOMBRE1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_NOMBRE2", lst.PARAM_NOMBRE2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_APELLIDO1", lst.PARAM_APELLIDO1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_APELLIDO2", lst.PARAM_APELLIDO2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_TIPO_DOCUMENTO", lst.PARAM_TIPO_DOCUMENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_NO_DOCUMENTO", lst.PARAM_NO_DOCUMENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_SEXO", lst.PARAM_SEXO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_FECHA_NACIMIENTO", lst.PARAM_FECHA_NACIMIENTO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_CORREO_ELECTRONICO", lst.PARAM_CORREO_ELECTRONICO, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    dynamicParameters.Add("P_PARENTESCO", lst.PARAM_PARENTESCO, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);


                    var conn = this.GetConnection();
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {

                        var query = "SB.PR_REGISTRO_GRUPO_FAMILIAR";

                        SqlMapper.Query(conn, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);

                        var dato_grupo_familiar = "EXITOSO";

                        var arrayResponseGrupo = new RespuestaGrupoFamiliar
                        {
                            P_SALIDA_GRUPO_FAMILIAR = dato_grupo_familiar
                        };

                        saveGrupoFamiliar = arrayResponseGrupo;
                        saveGrupoFamiliarArray.Add(arrayResponseGrupo);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return saveGrupoFamiliarArray;
        }

        public List<ModeloAnalisis> GetValidate(List<Analisis> analisis)
        {

            List<ModeloAnalisis> validateInfoArray = new List<ModeloAnalisis>();

            try
            {
                foreach (Analisis lst in analisis)
                {
                    var dynamicParamValidate= new OracleDynamicParameters();

                    dynamicParamValidate.Add("P_DOCUMENTO_PERSONA", lst.PARAM_DOCUMENTO_PERSONA, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_CALLE", lst.PARAM_CALLE, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO1", lst.PARAM_NUMERO1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA1", (lst.PARAM_LETRA1), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_BIS1", lst.PARAM_BIS1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA2", lst.PARAM_LETRA2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_ESTESUR1", lst.PARAM_ESTESUR1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO2", lst.PARAM_NUMERO2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA3", lst.PARAM_LETRA3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_BIS2", lst.PARAM_BIS2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA4", lst.PARAM_LETRA4, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO3", lst.PARAM_NUMERO3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_ESTESUR2", lst.PARAM_ESTESUR2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_VALIDACION", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    dynamicParamValidate.Add("P_IDENTIFICADOR", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                    dynamicParamValidate.Add("P_CUNU", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("P_DIRECCION", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);

                    dynamicParamValidate.Add("R_NOMBARIO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("R_LOCALIDAD", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("R_ESTRATO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);

                    var conn = this.GetConnection();
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {

                        var query = "SB.PR_ANALISIS_REGISTRO_SOLICITUD";
                        SqlMapper.Query(conn, query, param: dynamicParamValidate, commandType: CommandType.StoredProcedure);

                        var P_VALIDACION_TEMP = dynamicParamValidate.Get<Int32>("P_VALIDACION");
                        var P_IDENTIFICADOR_TEMP = dynamicParamValidate.Get<Int32>("P_IDENTIFICADOR");
                        var P_CUNU_TEMP = dynamicParamValidate.Get<String>("P_CUNU");
                        var P_DIRECCION_TEMP = dynamicParamValidate.Get<String>("P_DIRECCION");

                        var P_R_NOMBARIO_TEMP = dynamicParamValidate.Get<String>("R_NOMBARIO");

                        var P_R_LOCALIDAD_TEMP = dynamicParamValidate.Get<String>("R_LOCALIDAD");

                        var P_R_ESTRATO_TEMP = dynamicParamValidate.Get<String>("R_ESTRATO");


                        var arrayResponseValidate = new ModeloAnalisis
                        {
                            P_VALIDACION = P_VALIDACION_TEMP,
                            P_IDENTIFICADOR = P_IDENTIFICADOR_TEMP,
                            P_CUNU = P_CUNU_TEMP,
                            P_DIRECCION = P_DIRECCION_TEMP,
                            P_ID_CIUDANANO = lst.PARAM_DOCUMENTO_PERSONA,
                            P_R_NOMBARIO = P_R_NOMBARIO_TEMP,
                            P_R_LOCALIDAD = P_R_LOCALIDAD_TEMP,
                            P_R_ESTRATO = P_R_ESTRATO_TEMP,

                        };

                        validateInfoArray.Add(arrayResponseValidate);
                    }
                }

            }


            catch (Exception ex)
            {
                throw ex;
            }

            return validateInfoArray;
        }

        public object GetValidateSolicitante(Analisis analisis)
        {
            object saveValidateSolicitante = null;

            try
            {
                    var dynamicParamValidate = new OracleDynamicParameters();

                    dynamicParamValidate.Add("P_DOCUMENTO_PERSONA", analisis.PARAM_DOCUMENTO_PERSONA, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_CALLE", analisis.PARAM_CALLE, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO1", analisis.PARAM_NUMERO1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA1", (analisis.PARAM_LETRA1), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_BIS1", analisis.PARAM_BIS1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA2", analisis.PARAM_LETRA2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_ESTESUR1", analisis.PARAM_ESTESUR1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO2", analisis.PARAM_NUMERO2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA3", analisis.PARAM_LETRA3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_BIS2", analisis.PARAM_BIS2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_LETRA4", analisis.PARAM_LETRA4, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_NUMERO3", analisis.PARAM_NUMERO3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_ESTESUR2", analisis.PARAM_ESTESUR2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, size: 50);
                    dynamicParamValidate.Add("P_VALIDACION", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    dynamicParamValidate.Add("P_IDENTIFICADOR", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                    dynamicParamValidate.Add("P_CUNU", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("P_DIRECCION", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);

                    dynamicParamValidate.Add("R_NOMBARIO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("R_LOCALIDAD", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);
                    dynamicParamValidate.Add("R_ESTRATO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Output, size: 100);


                var conn = this.GetConnection();
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {

                        var query = "SB.PR_ANALISIS_REGISTRO_SOLICITUD";
                        SqlMapper.Query(conn, query, param: dynamicParamValidate, commandType: CommandType.StoredProcedure);

                        var P_VALIDACION_TEMP = dynamicParamValidate.Get<Int32>("P_VALIDACION");
                        var P_IDENTIFICADOR_TEMP = dynamicParamValidate.Get<Int32>("P_IDENTIFICADOR");
                        var P_CUNU_TEMP = dynamicParamValidate.Get<String>("P_CUNU");
                        var P_DIRECCION_TEMP = dynamicParamValidate.Get<String>("P_DIRECCION");

                        var P_R_NOMBARIO_TEMP = dynamicParamValidate.Get<String>("R_NOMBARIO");

                        var P_R_LOCALIDAD_TEMP = dynamicParamValidate.Get<String>("R_LOCALIDAD");

                        var P_R_ESTRATO_TEMP = dynamicParamValidate.Get<String>("R_ESTRATO");

                        var arrayResponseValidateSolicitante = new ModeloAnalisis
                        {
                            P_VALIDACION = P_VALIDACION_TEMP,
                            P_IDENTIFICADOR = P_IDENTIFICADOR_TEMP,
                            P_CUNU = P_CUNU_TEMP,
                            P_DIRECCION = P_DIRECCION_TEMP,
                            P_ID_CIUDANANO = analisis.PARAM_DOCUMENTO_PERSONA,
                            P_R_NOMBARIO = P_R_NOMBARIO_TEMP,
                            P_R_LOCALIDAD = P_R_LOCALIDAD_TEMP,
                            P_R_ESTRATO = P_R_ESTRATO_TEMP,

                        };

                    saveValidateSolicitante = arrayResponseValidateSolicitante;
                }
            }


            catch (Exception ex)
            {
                throw ex;
            }

            return saveValidateSolicitante;
        }

    }
}
