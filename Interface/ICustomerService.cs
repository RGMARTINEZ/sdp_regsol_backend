﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Models;

namespace WebApiOracle.Interface
{
    public interface ICustomerService
    {

        //object GetCustomerByIdPrincipal(int id);

        //object GetCustomerByIdPrincipal(Analisis analisis);

        object AddSolicitante(ModeloSaveSolicitante modeloSaveSolicitante);

        List<ModeloAnalisis> GetValidate(List<Analisis> analisis);

        object GetValidateSolicitante(Analisis analisis);

        List<RespuestaGrupoFamiliar> AddGrupoFamiliar(List<ModeloSaveGrupoFamiliar> modeloSaveGrupoFamiliar);

        object GetListCustomer();
    }
}
