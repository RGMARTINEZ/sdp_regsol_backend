﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiOracle.Utility
{
    public class SwaggerOptions
    {
        public string Description { get; set; }
        public string JsonRoute { get; set; }
        public string UiEndpoint { get; set; }
    }
}
