﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiOracle.Interface;
using WebApiOracle.Models;
using WebApiOracle.Utility;

namespace WebApiOracle.Controllers
{
    //[Route("api/v1/[controller]")]

    [EnableCors("MyPolicy")]
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]

    public class CustomerController : ControllerBase
    {
        // Se inyecta el servicio mediante la referecnia de la interfaz
        ICustomerService service;

        public CustomerController(ICustomerService customerService) {
            service = customerService;
        }


        [HttpGet]
        public ActionResult GetAllCustomer() {

            var result = service.GetListCustomer();

            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        /*[HttpPost]
        public ActionResult create(Customer customer) {
            var resp = new Dictionary<string, bool>();
            try
            {
                service.AddCustomer(customer);
                resp.Add("created", true);
            }
            catch (Exception)
            {
                resp.Add("created", false);
            }
            return Ok(resp);

        } */

        [HttpPost("saveSolicitante")]
        public ActionResult createSolicitud(ModeloSaveSolicitante modeloSaveSolicitante)
        {
            var result = service.AddSolicitante(modeloSaveSolicitante);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);

        }

        [HttpPost("saveGrupoFamiliar")]
        public ActionResult createGrupoFamiliar(List<ModeloSaveGrupoFamiliar> modeloSaveGrupoFamiliar)
        {
            var result = service.AddGrupoFamiliar(modeloSaveGrupoFamiliar);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);

        }


        [HttpPost("validateGrupoFamiliar")]
        public ActionResult createValidacionGrupo(List<Analisis> analisis)
        {
            var result = service.GetValidate(analisis);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);

        }


        [HttpPost("validateSolicitante")]
        public ActionResult createValidacionSolicitante(Analisis analisis)
        {
            var result = service.GetValidateSolicitante(analisis);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);

        }


    }
}
