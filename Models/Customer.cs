﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WebApiOracle.Models
{
    public class Customer
    {
         
        public string InternoCiudadano { get; set; }

        public string FechaNacimiento { get; set; }

        public string TipoDocumento { get; set; }

        public string NumeroDocumento { get; set; }

        public string PrimerApellido { get; set; }

        public string SegundoApellido { get; set; }

        public string PrimerNombre { get; set; }
         
        public string SegundoNombre { get; set; }

        public string Sexo { get; set; }

        public string CorreoElectronico { get; set; }

        public string Parentesco { get; set; }
         
    }
}
