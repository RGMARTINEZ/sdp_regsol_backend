﻿using System;

namespace WebApiOracle
{
    public class ModeloSaveSolicitante
    {

        public string PARAM_NOMBRE1 { get; set; }

        public string PARAM_NOMBRE2 { get; set; }

        public string PARAM_APELLIDO1 { get; set; }

        public string PARAM_APELLIDO2 { get; set; }

        public string PARAM_TIPO_DOCUMENTO { get; set; }

        public string PARAM_NO_DOCUMENTO { get; set; }

        public string PARAM_SEXO { get; set; }

        public string PARAM_FECHA_NACIMIENTO { get; set; }

        public string PARAM_CORREO_ELECTRONICO { get; set; }

        public string PARAM_TELEFONO { get; set; }

        public Int32 PARAM_PARENTESCO { get; set; }

        public string PARAM_CUNU { get; set; }

        public string PARAM_DIRECCION { get; set; }


    }
}
