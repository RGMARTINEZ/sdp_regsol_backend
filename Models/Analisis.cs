﻿using System;

namespace WebApiOracle
{
    public class Analisis
    {

        public string PARAM_DOCUMENTO_PERSONA { get; set; }

        public string PARAM_CALLE { get; set; }

        public string PARAM_NUMERO1 { get; set; }

        public string PARAM_LETRA1 { get; set; }

        public string PARAM_BIS1 { get; set; }

        public string PARAM_LETRA2 { get; set; }

        public string PARAM_ESTESUR1 { get; set; }

        public string PARAM_NUMERO2 { get; set; }

        public string PARAM_LETRA3 { get; set; }

        public string PARAM_BIS2 { get; set; }

        public string PARAM_LETRA4 { get; set; }

        public string PARAM_NUMERO3 { get; set; }

        public string PARAM_ESTESUR2 { get; set; }

    }
}
