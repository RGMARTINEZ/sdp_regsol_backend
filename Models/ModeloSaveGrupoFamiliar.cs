﻿using System;

namespace WebApiOracle
{
    public class ModeloSaveGrupoFamiliar
    {
        public Int32 PARAM_INTERNO_CIUDADANO { get; set; }

        public string PARAM_NOMBRE1 { get; set; }

        public string PARAM_NOMBRE2 { get; set; }

        public string PARAM_APELLIDO1 { get; set; }

        public string PARAM_APELLIDO2 { get; set; }

        public string PARAM_TIPO_DOCUMENTO { get; set; }

        public string PARAM_NO_DOCUMENTO { get; set; }

        public string PARAM_SEXO { get; set; }

        public string PARAM_FECHA_NACIMIENTO { get; set; }

        public string PARAM_CORREO_ELECTRONICO { get; set; }

        public Int32 PARAM_PARENTESCO { get; set; }

    }
}
